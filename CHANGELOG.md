# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/compare/v1.0.0...v1.1.0) (2024-10-14)


### Features

* **Dockerfile:** Update to Node v20 and semantic-release v24 ([008c7e3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/008c7e30aafc4c1b6d2540c6dd90238e33681dc1))

# 1.0.0 (2023-12-27)


### Bug Fixes

* **ci:** Move to Open CoDE ([eef87b6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/eef87b6d34ebcc7ae81d29d6c37f1295f4440548))
* **dockerfile:** Add pinned versions of semantic-release ([2f3dd5d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/2f3dd5da8b4571c4a59a3d53d7c811f49520cab3))
* **Dockerfile:** Change to node:18 ([aab8c73](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/aab8c73c6149769dc4a3d7f7f59dabe8a376a68d))


### Features

* **docker:** add semantic-release dockerfile and ci pipeline ([b3a3f7f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/b3a3f7fb905c29aebdeb90eb64ea60425b48afff))
* **Dockerfile:** Add @semantic-release/git ([4bae6e6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/4bae6e6db960759a8536d51ebef2b5f4442495be))
* **Dockerfile:** add ca-certificates package ([d52080d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/d52080d509e0e9b0dc8e11913e0b214195d0b9d6))
* **docs:** Add licenses and README ([6eb2901](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release/commit/6eb2901d0f89b5ad652389c745b65df3215aeb26))
