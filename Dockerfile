# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM docker.io/node:20.18.0-bullseye-slim

RUN apt-get update -qq \
 && apt-get -qq -y install --no-install-recommends \
    git \
    git-lfs \
    curl \
    ca-certificates \
 && apt-get upgrade -y \
 # Install semantic release
 && npm install -g \
    semantic-release@24.1.2 \
    @semantic-release/gitlab@13.2.1 \
    @semantic-release/git@10.0.1 \
    @semantic-release/release-notes-generator@14.0.1 \
    @semantic-release/changelog@6.0.3 \
 && apt-get -y autoremove \
 && apt-get -y clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/cache/apt/archives/*
